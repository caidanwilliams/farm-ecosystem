package com.psychedelicpelican.farmecosystem.entities.Animals;

import com.psychedelicpelican.farmecosystem.entities.Entity;
import com.psychedelicpelican.farmecosystem.entities.Food.Food;

import java.util.Random;

/**
 * Created by Caidan Williams on 5/6/2016.
 */
public class Animal implements Entity {

    protected int xPos = 0, yPos = 0; // Certain location on the map/land
    protected int health = 10; // For living and dying
    protected double weight = health * 3.5;
    protected boolean canHarvest = false; // Milk cows, gather eggs, etc.
    protected boolean canButcher = false; // Ready for butchering and selling/eating

    // Animal eats, to increase health and help produce harvest
    public void eat(Food food) {
        if (food.isNear(this) && food.isEdible()) {
            food.eat(this);
        }
    }

    public void move() {
        Random random = new Random();

        // Move the x pos randomly
        double rand = random.nextDouble();
        if (rand > 0.7) {
            // Todo: check to make sure its on the map
            setXPos(getXPos() + 1);
        }
        else if (rand < 0.3) {
            setXPos(getXPos() - 1);
        }

        // Move the y pos randomly
        rand = random.nextDouble();
        if (rand > 0.7) {
            setYPos(getXPos() + 1);
        }
        else if (rand < 0.3) {
            setYPos(getYPos() - 1);
        }
    }

    public int getXPos() {
        return xPos;
    }

    public void setXPos(int x) {
        xPos = x;
    }

    public int getYPos() {
        return yPos;
    }

    public void setYPos(int y) {
        yPos = y;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
        setWeight(this.health);
    }

    public void addHealth(int health) {
        this.health += health;
        setWeight(this.health);
    }

    public void subtractHealth(int health) {
        this.health -= health;
        setWeight(this.health);
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(int health) {
        this.weight = health * 3.5;
    }

    public boolean isCanHarvest() {
        return canHarvest;
    }

    public void setCanHarvest(boolean canHarvest) {
        this.canHarvest = canHarvest;
    }

    public boolean isCanButcher() {
        return canButcher;
    }

    public void setCanButcher(boolean canButcher) {
        this.canButcher = canButcher;
    }

}
