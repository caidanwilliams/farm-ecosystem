package com.psychedelicpelican.farmecosystem.entities.Food;

import com.psychedelicpelican.farmecosystem.entities.Animals.Animal;

/**
 * Created by Caidan Williams on 5/6/2016.
 */
public class Grass extends Food {

    @Override
    public boolean isNear(Animal animal) {
        return false;
    }
}
