package com.psychedelicpelican.farmecosystem.entities.Food;

import com.psychedelicpelican.farmecosystem.entities.Animals.Animal;

/**
 * Created by Caidan Williams on 5/6/2016.
 */
public abstract class Food {

    int xPos = 0, yPos = 0;
    int state = 2; // 0 = dead, 1 = growing, 2 = healthy/fully grown
    int nutrition = 1; // default value

    public boolean isNear(Animal animal) {
        if (animal.getXPos() == xPos && animal.getYPos() == yPos) { // Check the x and y position of the food relative to the animal
            return true;
        }
        return false;
    }

    public boolean isEdible() {
        if (state == 2) {
            return true;
        }
        return false;
    }

    public void eat(Animal animal) {
        animal.addHealth(nutrition);
        state = 0;
    }
}
