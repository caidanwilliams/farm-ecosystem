package com.psychedelicpelican.farmecosystem.entities;

/**
 * Created by Caidan Williams on 7/19/2016.
 */
public interface Entity {

    int age = 0; // Days alive

    // Used to update all entities at the end of every day
    default void update() {

    }

}
