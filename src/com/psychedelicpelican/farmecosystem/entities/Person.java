package com.psychedelicpelican.farmecosystem.entities;

import jalse.entities.Entity;
import jalse.entities.annotations.GetAttribute;
import jalse.entities.annotations.SetAttribute;

/**
 * Created by Caidan Williams on 5/6/2016.
 */
public interface Person extends Entity {

    @GetAttribute
    String getName();

    @SetAttribute
    Void setName(String name);

}
