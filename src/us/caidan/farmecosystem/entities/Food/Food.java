package us.caidan.farmecosystem.entities.Food;

import jalse.entities.Entity;
import jalse.entities.annotations.GetAttribute;

/**
 * Created by Caidan Williams on 5/6/2016.
 */
public interface Food extends Entity {

    int state = 2; // 0 = dead, 1 = growing, 2 = healthy
    int nutrition = 1; // default value

    @GetAttribute
    int getNutrition();

}
