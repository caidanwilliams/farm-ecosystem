package us.caidan.farmecosystem.entities.Animals;

import jalse.entities.Entity;
import us.caidan.farmecosystem.entities.Food.Food;

/**
 * Created by Caidan Williams on 5/6/2016.
 */
public interface Animal extends Entity {

    int xPos = 0, yPos = 0; // Certain location on the map/land
    int health = 10; // For living and dying
    boolean harvest = false; // Milk cows, gather eggs, etc.
    boolean butcher = false; // Ready for butchering and selling/eating

    // Animal eats, to increase health and help produce harvest
    default void eat(Food food) {
        //health += food.getNutrition();
        killEntity(food.getID());
    }

}
